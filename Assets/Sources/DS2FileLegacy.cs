﻿using UnityEngine;
using UnityEditor;
using System;
using System.IO;
using System.Collections.Generic;

namespace DS2Engine
{
	public class DS2FileLegacy : DS2File
    {
		private Stack<string> m_stack = new Stack<string>();
		private Stack<string> m_lineInfo = new Stack<string>();

		public DS2FileLegacy(BinaryReader reared, string path, bool debug = false)
		{
			m_path = path;
			m_isDebug = debug;

			int size = (int)reared.BaseStream.Length;
			string text = new string(reared.ReadChars(size));
			reared.Close();

			var lines = text.Split(new[] { "\r\n", "\r", "\n" }, StringSplitOptions.RemoveEmptyEntries);
			for (int i = lines.Length; i > 0; i--)
			{
				var values = lines[i - 1].Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
				for (int j = values.Length; j > 0; j--)
				{
					m_stack.Push(values[j - 1]);
					m_lineInfo.Push(i + ":" + j);
				}
			}

			format = ReadString();
			version = ReadString();

			if (m_isDebug)
				Debug.LogFormat("{0} version '{1}' {2}", format, version, m_path);
		}

		public override void Close()
		{
			if (m_stack.Count == 0 && m_lineInfo.Count == 0)
				return;

			if (m_stack.Count > 0 || m_lineInfo.Count > 0)
				Debug.LogErrorFormat("Close file when reading is not complete {0} {1}", m_stack.Count, m_lineInfo.Count);

			if (m_isDebug)
				Debug.LogFormat("Close {0}", m_path);

			m_stack.Clear();
			m_lineInfo.Clear();
		}

		public override void PrintDebug(string valueType)
		{
			if (m_isDebug && !m_isMuted)
				Debug.LogWarningFormat("Try read {0} at position {1}", valueType, m_lineInfo.Peek());
		}

		public override string ReadString()
		{
			PrintDebug("String");

			var value = m_stack.Pop();
			var lineInfo = m_lineInfo.Pop();
			if (!value.StartsWith("\"") || !value.EndsWith("\""))
				Abort("Value '{0}' is not string at line: {1}", value, lineInfo);

			return value.Substring(1, value.Length - 2);
		}

		public override uint ReadUInt16()
		{
			PrintDebug("UInt16");

			var value = m_stack.Pop();
			var lineInfo = m_lineInfo.Pop();
			if (value.Contains(".") || value.Contains("\""))
				Abort("Value '{0}' is not integer at line: {1}", value, lineInfo);

			return (uint)Convert.ToUInt16(value);
		}

		public override float ReadFloat()
		{
			PrintDebug("Float");

			var value = m_stack.Pop();
			var lineInfo = m_lineInfo.Pop();
			if (!value.Contains(".") || value.Contains("\""))
				Abort("Value '{0}' is not float at line: {1}", value, lineInfo);

			return Convert.ToSingle(value);
		}
    }
}