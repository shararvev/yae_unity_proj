﻿using UnityEngine;
using UnityEditor;
using System;
using System.IO;
using System.Collections.Generic;

namespace DS2Engine
{
	public static class DS2Format
	{
		public static DS2File OpenFile(string path, bool debug = false)
		{
			if (debug)
				Debug.LogFormat("Open {0}", path);

			var fileSteam = new FileStream(path, FileMode.Open, FileAccess.Read);
			var reader = new BinaryReader(fileSteam);

			string textHeader = new string(reader.ReadChars(16));
			reader.BaseStream.Position = 0;

			if (textHeader == "\"DS2ModelFile_1\"")
				return new DS2FileLegacy(reader, path, debug);

			return new DS2File(reader, path, debug);
		}

		public static void Parse05(DS2File file)
		{
			var meshCount = file.ReadUInt16();

			for (int i = 0; i < meshCount; i++)
			{
				var mesh = new DS2Mesh();

				mesh.name = file.ReadString();
				mesh.material = file.ReadString();
				mesh.texture1 = file.ReadString();
				mesh.texture2 = file.ReadString();
				mesh.texture3 = file.ReadString();
				mesh.texture4 = file.ReadString();
				mesh.texture5 = file.ReadString();

				var triangleCount = file.ReadUInt16();
				var vertexCount = file.ReadUInt16();
				var indexCount = file.ReadUInt16();

				if (indexCount != triangleCount * 3)
					throw new Exception("indexCount and triangleCount not equivalent");

				// zero on all 18 files
				var unknownZero1 = file.ReadUInt16();
				if (unknownZero1 != 0)
					throw new Exception("unknownZero1 not 0");

				mesh.vertices = new Vector3[vertexCount];
				mesh.normals = new Vector3[vertexCount];
				mesh.uv = new Vector2[vertexCount];
				for (int j = 0; j < vertexCount; j++)
				{
					mesh.vertices[j] = file.ReadVector3();
					mesh.normals[j] = file.ReadNormal();
					mesh.uv[j] = file.ReadVector2();
				}

				mesh.indices = new int[indexCount];
				for (int j = 0; j < indexCount; j++)
					mesh.indices[j] = (int)file.ReadUInt16();

				// animations
				var matrixCount = file.ReadUInt16();
				float animationFps = file.ReadFloat();

				if (matrixCount > 0)
				{
					for (int j = 0; j < matrixCount; j++)
					{
						var aabb = file.ReadAABB();
					}
				}
				else
				{
					var aabb = file.ReadAABB();
				}

				// zero on all 18 files
				var unknownZero2 = file.ReadUInt16();
				if (unknownZero2 != 0)
					throw new Exception("unknownZero2 not 0");

				// zero on 17 files and 1 on file with matrices
				var unknownBool = file.ReadUInt16();
				if (matrixCount > 0 && unknownBool != 1)
					throw new Exception("unknownBool not 1");
				if (matrixCount == 0 && unknownBool != 0)
					throw new Exception("unknownBool not 0");

				for (int j = 0; j < matrixCount; j++)
				{
					var transformMatrix = file.ReadMatrix4x4();
					//Debug.LogFormat("mat: {0}", transformMatrix);
				}

				// zero on all 18 files
				var unknownZero3 = file.ReadUInt16();
				if (unknownZero3 != 0)
					throw new Exception("unknownZero3 not 0");

				var umesh = new Mesh();
				umesh.vertices = mesh.vertices;
				umesh.triangles = mesh.indices;
				umesh.uv = mesh.uv;
				umesh.normals = mesh.normals;

				GameObject model = new GameObject("mesh" + i);
				model.AddComponent<MeshFilter>().sharedMesh = umesh;
				model.AddComponent<MeshRenderer>().sharedMaterial = (Material)Resources.Load("Default", typeof(Material)); ;
			}

			// zero on all 18 files
			var unknownZero4 = file.ReadUInt16();
			if (unknownZero4 != 0)
				throw new Exception("unknownZero4 not 0");
		}
	}

#if false

    public class DS2
    {
        static Dictionary<string, string> DS2Formats = new Dictionary<string, string>
        {
            {"DS2GeometryFile_1", "0.8"},
        };

        void ParseHeader(BinaryReader r)
        {
            var id = ReadString(r);
            var version = ReadString(r);
            Debug.Log(string.Format("Header: {0}, {1}", id, version));

            string supportedVersion;
            if (!DS2Formats.TryGetValue(id, out supportedVersion) || version != supportedVersion)
                throw new Exception("Not supported format");
        }

        struct DS2Mesh
        {
            public bool isDynamic;
            public string material;
            public string texture1;
            public string texture2;
            public string texture3;
            public string texture4;
            public string texture5;

            public Bounds aabb;
            public Vector3 _dirty_origin;
            public float _dirty_volume;
            public Vector3 _dirty_center;

            public Vector3 _rotate_origin;
            public Vector3 _rotate_x;
            public Vector3 _rotate_y;
            public Vector3 _rotate_z;

            public Vector3 obb1;
            public Vector3 obb2;
            public Vector3 obb3;
            public Vector3 obb4;
            public Vector3 obb5;
            public Vector3 obb6;
            public Vector3 obb7;
            public Vector3 obb8;

            public bool _use_attr;
            public Vector3 _attr_nvec;

            public int _unknownNumber;
            public int triangleCount;
            public int vertexCount;
            public int _indexCount;
            public int _indexCount2;

            public int bufferIdxId;
            public int bufferIdxOffset;
            public int bufferVtxId;
            public int bufferVtxOffset;

            public int _dirty1;
            public int _dirty2;
            public int _dirty3;
            public int _dirty4;
            public int _dirty5;
            public int _dirty6;
            public int _dirty7;
            public int _dirty8;
            public int _dirty9;
            public int _dirty10;
            public int _dirty11;
            public int _dirty12;
            public int _dirty13;
            public int _dirty14;
            public int _dirty15;
            public int _dirty16;
            public int _dirty17;
            public int _dirty18;
            public int _dirty19;
            public int _dirty20;
            public int _dirty21;
            public int _dirty22;
            public int _dirty23;
            public int _dirty24;
            public int _dirty25;
            public int _dirty26;
            public int _dirty27;
        }

        DS2Mesh GetMesh(BinaryReader r)
        {
            DS2Mesh mesh = new DS2Mesh();

            mesh.isDynamic = r.ReadBoolean();
            mesh.material = ReadString(r);
            mesh.texture1 = ReadString(r);
            mesh.texture2 = ReadString(r);
            mesh.texture3 = ReadString(r);
            mesh.texture4 = ReadString(r);
            mesh.texture5 = ReadString(r);

            Debug.Log(string.Format("{0}:\t {1}, {2}, {3}, {4}, {5}, {6}, {7}", r.BaseStream.Position, mesh.isDynamic, mesh.material, mesh.texture1, mesh.texture2, mesh.texture3, mesh.texture4, mesh.texture5));

            mesh.aabb = ReadAABB(r);
            mesh._dirty_origin = ReadVector3(r);
            mesh._dirty_volume = r.ReadSingle();
            mesh._dirty_center = ReadVector3(r);

            Debug.Log(string.Format("{0}:\t aabb {1}", r.BaseStream.Position, mesh.aabb));
            Debug.Log(string.Format("{0}:\t origin {1}, volume {2}", r.BaseStream.Position, mesh._dirty_origin, mesh._dirty_volume));
            Debug.Log(string.Format("{0}:\t center {1}", r.BaseStream.Position, mesh._dirty_center));

            mesh._rotate_origin = ReadVector3(r);
            mesh._rotate_x = ReadVector3(r);
            mesh._rotate_y = ReadVector3(r);
            mesh._rotate_z = ReadVector3(r);

            Debug.Log(string.Format("{0}:\t rot {1}, {2}, {3}, {4}", r.BaseStream.Position, mesh._rotate_origin, mesh._rotate_x, mesh._rotate_y, mesh._rotate_z));

            // top
            mesh.obb1 = ReadVector3(r);
            mesh.obb2 = ReadVector3(r);
            mesh.obb3 = ReadVector3(r);
            mesh.obb4 = ReadVector3(r);

            Debug.Log(string.Format("{0}:\t obbU {1}, {2}, {3}, {4}", r.BaseStream.Position, mesh.obb1, mesh.obb2, mesh.obb3, mesh.obb4));

            // down
            mesh.obb5 = ReadVector3(r);
            mesh.obb6 = ReadVector3(r);
            mesh.obb7 = ReadVector3(r);
            mesh.obb8 = ReadVector3(r);

            Debug.Log(string.Format("{0}:\t obbD {1}, {2}, {3}, {4}", r.BaseStream.Position, mesh.obb5, mesh.obb6, mesh.obb7, mesh.obb8));

            mesh._use_attr = r.ReadBoolean();
            mesh._attr_nvec = ReadVector3(r);

            Debug.Log(string.Format("{0}:\t attr {1}, {2}", r.BaseStream.Position, mesh._use_attr, mesh._attr_nvec));

            mesh._unknownNumber = r.ReadUInt32();
            mesh.triangleCount = r.ReadUInt32();
            mesh.vertexCount = r.ReadUInt32();
            mesh._indexCount = r.ReadUInt32();
            mesh._indexCount2 = r.ReadUInt32();

            Debug.Log(string.Format("{0}:\t cntrs {1}, {2}, {3}, {4}, {5}", r.BaseStream.Position, mesh._unknownNumber, mesh.triangleCount, mesh.vertexCount, mesh._indexCount, mesh._indexCount2));

            if (mesh.isDynamic)
            {
                mesh._dirty1 = r.ReadUInt32();
                mesh._dirty2 = r.ReadUInt32();
                mesh._dirty3 = r.ReadUInt32();
                mesh._dirty4 = r.ReadUInt32();
                mesh._dirty5 = r.ReadUInt32();
                mesh._dirty6 = r.ReadUInt32();
                mesh._dirty7 = r.ReadUInt32();
                mesh._dirty8 = r.ReadUInt32();
                mesh._dirty9 = r.ReadUInt32();
                mesh._dirty10 = r.ReadUInt32();
                mesh._dirty11 = r.ReadUInt32();
                mesh._dirty12 = r.ReadUInt32();
                mesh._dirty13 = r.ReadUInt32();
                mesh._dirty14 = r.ReadUInt32();
                mesh._dirty15 = r.ReadUInt32();
                mesh._dirty16 = r.ReadUInt32();
                mesh._dirty17 = r.ReadUInt32();
                mesh._dirty18 = r.ReadUInt32();
                mesh._dirty19 = r.ReadUInt32();
                mesh._dirty20 = r.ReadUInt32();
                mesh._dirty21 = r.ReadUInt32();
                mesh._dirty22 = r.ReadUInt32();
                mesh._dirty23 = r.ReadUInt32();
                mesh._dirty24 = r.ReadUInt32();
                mesh._dirty25 = r.ReadUInt32();
                mesh._dirty26 = r.ReadUInt32();
                mesh._dirty27 = r.ReadUInt32();

                Debug.Log(string.Format("{0}:\t dynbuf {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}", r.BaseStream.Position, mesh._dirty1, mesh._dirty2, mesh._dirty3, mesh._dirty4, mesh._dirty5, mesh._dirty6, mesh._dirty7, mesh._dirty8, mesh._dirty9, mesh._dirty10));
                Debug.Log(string.Format("{0}:\t dynbuf2 {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}", r.BaseStream.Position, mesh._dirty11, mesh._dirty12, mesh._dirty13, mesh._dirty14, mesh._dirty15, mesh._dirty16, mesh._dirty17, mesh._dirty18, mesh._dirty19, mesh._dirty20));
                Debug.Log(string.Format("{0}:\t dynbuf3 {1}, {2}, {3}, {4}, {5}, {6}, {7}", r.BaseStream.Position, mesh._dirty21, mesh._dirty22, mesh._dirty23, mesh._dirty24, mesh._dirty25, mesh._dirty26, mesh._dirty27));
            }
            else
            {
                mesh.bufferIdxId = r.ReadUInt32();
                mesh.bufferIdxOffset = r.ReadUInt32();
                mesh._dirty1 = r.ReadUInt32();
                mesh._dirty2 = r.ReadUInt32();
                mesh._dirty3 = r.ReadUInt32();
                mesh.bufferVtxId = r.ReadUInt32();
                mesh.bufferVtxOffset = r.ReadUInt32();

                Debug.Log(string.Format("{0}:\t buf {1}, {2}, {3}, {4}, {5}, {6}, {7}", r.BaseStream.Position, mesh.bufferIdxId, mesh.bufferIdxOffset, mesh._dirty1, mesh._dirty2, mesh._dirty3, mesh.bufferVtxId, mesh.bufferVtxOffset));
            }

            return mesh;
        }

        List<DS2Mesh> m_meshes = new List<DS2Mesh>();
        void ParseMeshes(BinaryReader r)
        {
            var meshCount = r.ReadUInt32();
            Debug.Log(string.Format("{0}:\t size {1}", r.BaseStream.Position, meshCount));

            for (int i = 0; i < meshCount; i++)
            {
                m_meshes.Add(GetMesh(r));
            }
        }

        [Flags]
        public enum DS2Signatures
        {
            Indices = 1 << 0,
            Vertices = 1 << 1,
            Normals = 1 << 2,
            Uv = 1 << 3,
            Uv2 = 1 << 4,
            Colors = 1 << 5,
            // я не встретил этих сигнатур
            Tangents = 1 << 6,
            Binormals = 1 << 7,
        }

        class DS2Buffer
        {
            public int signature;
            public int size;

            public int[] indices;
            public Vector3[] vertices;
            public Vector2[] uv;
            public Vector2[] uv2;
            public Color32[] colors;
            public Vector3[] normals;
            public Vector3[] tangents;
            public Vector3[] binormals;

            public bool HasIndices()
            {
                return (signature & (int)DS2Signatures.Indices) != 0;
            }
            public bool HasVertices()
            {
                return (signature & (int)DS2Signatures.Vertices) != 0;
            }
            public bool HasNormals()
            {
                return (signature & (int)DS2Signatures.Normals) != 0;
            }
            public bool HasUv()
            {
                return (signature & (int)DS2Signatures.Uv) != 0;
            }
            public bool HasUv2()
            {
                return (signature & (int)DS2Signatures.Uv2) != 0;
            }
            public bool HasColors()
            {
                return (signature & (int)DS2Signatures.Colors) != 0;
            }
            public bool HasTangents()
            {
                return (signature & (int)DS2Signatures.Tangents) != 0;
            }
            public bool HasBinormals()
            {
                return (signature & (int)DS2Signatures.Binormals) != 0;
            }
        }

        DS2Buffer GetBuffer(BinaryReader r)
        {
            DS2Buffer buffer = new DS2Buffer();

            buffer.signature = r.ReadUInt32();
            buffer.size = r.ReadUInt32();

            Debug.Log(string.Format("{0}:\t Buffer sign: {1}, size: {2}", r.BaseStream.Position, Convert.ToString(buffer.signature, 2), buffer.size));

            // Для каждой компоненты нужен отдельный цикл, потому что они идут друг за другом в строго определенном порядке.
            if (buffer.HasIndices())
            {
                buffer.indices = new int[buffer.size];
                for (int i = 0; i < buffer.size; i++)
                    buffer.indices[i] = r.ReadUInt32();
            }
            else
            {
                if (buffer.HasVertices())
                {
                    buffer.vertices = new Vector3[buffer.size];
                    for (int i = 0; i < buffer.size; i++)
                        buffer.vertices[i] = ReadVector3(r);
                }
                if (buffer.HasUv())
                {
                    buffer.uv = new Vector2[buffer.size];
                    for (int i = 0; i < buffer.size; i++)
                        buffer.uv[i] = ReadVector2(r);
                }
                if (buffer.HasUv2())
                {
                    buffer.uv2 = new Vector2[buffer.size];
                    for (int i = 0; i < buffer.size; i++)
                        buffer.uv2[i] = ReadVector2(r);
                }
                if (buffer.HasColors())
                {
                    buffer.colors = new Color32[buffer.size];
                    for (int i = 0; i < buffer.size; i++)
                        buffer.colors[i] = ReadColor32(r);
                }
                if (buffer.HasNormals())
                {
                    buffer.normals = new Vector3[buffer.size];
                    for (int i = 0; i < buffer.size; i++)
                        buffer.normals[i] = ReadVector3(r);
                }
                if (buffer.HasTangents())
                {
                    buffer.tangents = new Vector3[buffer.size];
                    for (int i = 0; i < buffer.size; i++)
                        buffer.tangents[i] = ReadVector3(r);
                }
                if (buffer.HasBinormals())
                {
                    buffer.binormals = new Vector3[buffer.size];
                    for (int i = 0; i < buffer.size; i++)
                        buffer.binormals[i] = ReadVector3(r);
                }
            }

            return buffer;
        }


        List<DS2Buffer> m_buffers = new List<DS2Buffer>();
        void ParseBuffers(BinaryReader r)
        {
            var buffersCount = r.ReadUInt32();
            Debug.Log(string.Format("{0}:\t size {1}", r.BaseStream.Position, buffersCount));

            for (int i = 0; i < buffersCount; i++)
            {
                m_buffers.Add(GetBuffer(r));
            }
        }
        void ParseLightmaps(BinaryReader r)
        {
        }
        void ParseVistree(BinaryReader r)
        {
        }
        void ParseLights(BinaryReader r)
        {
        }
        void ParseModels(BinaryReader r)
        {
        }

        void DS2Convert()
        {
            var filename = Application.dataPath + "/gameres/maps/map08/lastzlo.ds2";

            var r = new BinaryReader(File.Open(filename, FileMode.Open), System.Text.Encoding.ASCII);
            ParseHeader(r);

            string chunk_begin_key = "DS2Chunk_begin";
            string chunk_end_key = "DS2Chunk_end";

            while (r.BaseStream.Position < r.BaseStream.Length)
            {
                var chunk_begin = ReadString(r);
                Debug.Log(string.Format("{0}:\t chunk_begin {1}", r.BaseStream.Position, chunk_begin));

                if (chunk_begin != chunk_begin_key)
                    throw new Exception("Unknown chunk begin key");

                var chunk_id = ReadString(r);
                Debug.Log(string.Format("{0}:\t chunk_id {1}", r.BaseStream.Position, chunk_id));

                if (chunk_id == "meshes")
                    ParseMeshes(r);
                else if (chunk_id == "buffers")
                    ParseBuffers(r);
                else if (chunk_id == "lightmaps")
                    ParseLightmaps(r);
                else if (chunk_id == "vistree")
                    ParseVistree(r);
                else if (chunk_id == "lights")
                    ParseLights(r);
                else if (chunk_id == "models")
                    ParseModels(r);
                else
                    throw new Exception("Unknown chunk id");

                var chunk_end = ReadString(r);
                Debug.Log(string.Format("{0}:\t chunk_end {1}", r.BaseStream.Position, chunk_end));

                if (chunk_end != chunk_end_key)
                    throw new Exception("Unknown chunk end key");
            }

            r.Close();
            Debug.Log("complete");
        }
    }

#endif

}