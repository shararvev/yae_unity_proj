﻿using UnityEngine;
using UnityEditor;
using System;
using System.IO;
using System.Collections.Generic;

namespace DS2Engine
{
    public class DS2Mesh
	{
		public string name;
		public string material;
		public string texture1;
		public string texture2;
		public string texture3;
		public string texture4;
		public string texture5;

		public int[] indices;
        public Vector3[] vertices;
        public Vector2[] uv;
		//public Vector2[] uv2;
		//public Color32[] colors32;
        public Vector3[] normals;
		//public Vector3[] tangents;
		//public Vector3[] binormals;
    }
}