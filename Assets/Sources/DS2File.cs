﻿using UnityEngine;
using UnityEditor;
using System;
using System.IO;
using System.Collections.Generic;

namespace DS2Engine
{
    public class DS2File
	{
		private BinaryReader m_reader;

        protected string m_path;
		protected bool m_isDebug;
		protected bool m_isMuted;

		public string format;
		public string version = "none";

		// for inherit
		public DS2File()
		{
		}

		~DS2File()
		{
			Close();
		}

		public void Abort(string format, params object[] args)
		{
			Close();
			string message = string.Format(format, args);
			Debug.LogErrorFormat("Abort read process '{0}' by:\n\t{1}", m_path, message);
			throw new Exception();
		}

		public DS2File(BinaryReader reared, string path, bool debug = false)
		{
			m_reader = reared;
			m_path = path;
			m_isDebug = debug;

			format = ReadString();

			if (format != "DS2CollisionMap" && format != "DS2CameraTrack_1")
				version = ReadString();

			if (m_isDebug)
				Debug.LogFormat("{0} version '{1}' {2}", format, version, m_path);
		}

		public virtual void Close()
		{
			if (!m_reader.BaseStream.CanRead)
				return;

			if (m_reader.BaseStream.Position < m_reader.BaseStream.Length)
				Debug.LogErrorFormat("Close file when reading is not complete {0}", m_reader.BaseStream.Position);

			if (m_isDebug)
				Debug.LogFormat("Close {0}", m_path);

			m_reader.Close();
		}

		public virtual void PrintDebug(string valueType)
		{
			if (m_isDebug && !m_isMuted)
				Debug.LogWarningFormat("Try read {0} at position {1}", valueType, m_reader.BaseStream.Position);
		}

		public virtual string ReadString()
		{
			PrintDebug("String");

			m_isMuted = true;
			int length = (int)ReadUInt16();
			m_isMuted = false;

			if (length > 128)
				Abort("Attempt to read too long string: {0}", length);

			return new string(m_reader.ReadChars(length));
		}

		public virtual uint ReadUInt16()
		{
			PrintDebug("UInt16");
            return (uint)m_reader.ReadUInt16();
        }

		public uint ReadUInt32()
		{
			PrintDebug("UInt32");
			return m_reader.ReadUInt32();
        }

		public virtual float ReadFloat()
		{
			PrintDebug("Float");
			return m_reader.ReadSingle();
		}

		public Vector2 ReadVector2()
		{
			PrintDebug("Vector2");
			m_isMuted = true;
			var x = ReadFloat();
			var y = ReadFloat();
			m_isMuted = false;
			return new Vector2(x, y);
		}

		public Vector3 ReadVector3()
		{
			PrintDebug("Vector3");
			m_isMuted = true;
			var x = ReadFloat();
			var y = ReadFloat();
			var z = ReadFloat();
			m_isMuted = false;
			return new Vector3(x, y, z);
		}

		public Vector4 ReadVector4()
		{
			PrintDebug("Vector4");
			m_isMuted = true;
			var x = ReadFloat();
			var y = ReadFloat();
			var z = ReadFloat();
			var w = ReadFloat();
			m_isMuted = false;
			return new Vector4(x, y, z, w);
		}

		public Matrix4x4 ReadMatrix4x4()
		{
			PrintDebug("Matrix4x4");
			m_isMuted = true;
			var column0 = ReadVector4();
			var column1 = ReadVector4();
			var column2 = ReadVector4();
			var column3 = ReadVector4();
			m_isMuted = false;

			if (column0.w != 0 || column1.w != 0 || column2.w != 0 || column3.w != 1)
				Abort("Matrix is not homogeneous: {0} {1} {2} {3}", column0, column1, column2, column3);

			return new Matrix4x4(column0, column1, column2, column3);
		}

		public Bounds ReadAABB()
		{
			PrintDebug("AABB");
			m_isMuted = true;
			var min = ReadVector3();
			var max = ReadVector3();
			m_isMuted = false;

			if (min.x > max.x || min.y > max.y || min.z > max.z)
				Abort("Two vectors is not AABB: {0} {1}", min, max);

			var aabb = new Bounds();
			aabb.SetMinMax(min, max);
			return aabb;
		}

		public Color32 ReadColor32()
		{
			PrintDebug("Color32");
			m_isMuted = true;
			var r = ReadUInt32();
			var g = ReadUInt32();
			var b = ReadUInt32();
			var a = ReadUInt32();
			m_isMuted = false;

			if (r > 255 || g > 255 || b > 255 || a > 255)
				Abort("Color component is greater than byte: {0} {1} {2} {3}", r, g, b, a);

			return new Color32((byte)r, (byte)g, (byte)b, (byte)a);
		}

		public Vector3 ReadNormal()
		{
			PrintDebug("Normal");
			m_isMuted = true;
			var normal = ReadVector3();
			m_isMuted = false;

			if (m_isDebug && (normal.magnitude > 1.001 || normal.magnitude < 0.999))
				Debug.LogErrorFormat("Normal vector is not normalized: {0} {1}", normal, normal.magnitude);

			normal.Normalize();
			return normal;
		}
    }
}