﻿using UnityEngine;
using UnityEditor;
using System;
using System.IO;
using System.Collections.Generic;
using DS2Engine;

public class Convertor : EditorWindow
{
    [MenuItem("YAE/Tollkit")]
    static void Init()
    {
        var window = (Convertor)EditorWindow.GetWindow(typeof(Convertor));
        window.Show();
    }

	string path = "/";
    void OnGUI()
    {
        if (GUILayout.Button("forall"))
        {
            DS2Convert();
		}
		path = EditorGUILayout.TextField(path);
		if (GUILayout.Button("parse test"))
		{
			ParseTest(path);
		}
    }

	void ParseTest(string path)
	{
		string[] files = {
			"E:/UnityProjects/yae_parser/Assets/gameres/models/models/sky/sky_333.ds2md",
			"E:/UnityProjects/yae_parser/Assets/gameres/models/models/sky/sky_77.ds2md",
			"E:/UnityProjects/yae_parser/Assets/gameres/models/models/sky/sky_a01.ds2md",
			"E:/UnityProjects/yae_parser/Assets/gameres/models/models/sky/sky_b03.ds2md",
			"E:/UnityProjects/yae_parser/Assets/gameres/models/models/sky/sky_botan.ds2md",
			"E:/UnityProjects/yae_parser/Assets/gameres/models/models/sky/sky_catsfergray.ds2md",
			"E:/UnityProjects/yae_parser/Assets/gameres/models/models/sky/sky_dek.ds2md",
			"E:/UnityProjects/yae_parser/Assets/gameres/models/models/sky/sky_dekoo.ds2md",
			"E:/UnityProjects/yae_parser/Assets/gameres/models/models/sky/sky_kol2.ds2md",
			"E:/UnityProjects/yae_parser/Assets/gameres/models/models/sky/sky_les.ds2md",
			"E:/UnityProjects/yae_parser/Assets/gameres/models/models/sky/sky_manold.ds2md",
			"E:/UnityProjects/yae_parser/Assets/gameres/models/models/sky/sky_poh.ds2md",
			"E:/UnityProjects/yae_parser/Assets/gameres/models/models/sky/sky_s2.ds2md",
			"E:/UnityProjects/yae_parser/Assets/gameres/models/models/sky/sky_s2000.ds2md",
			"E:/UnityProjects/yae_parser/Assets/gameres/models/models/sky/sky_sh3_.ds2md",
			"E:/UnityProjects/yae_parser/Assets/gameres/models/models/sky/sky_term_.ds2md",
			"E:/UnityProjects/yae_parser/Assets/gameres/models/models/sky/sky_wall_dyn.ds2md",
			"E:/UnityProjects/yae_parser/Assets/gameres/models/models/vent.ds2md",
		};
		foreach (string filename in files){
			DS2File file = DS2Format.OpenFile(filename);
			DS2Format.Parse05(file);
			file.Close();
		}
	}

    void DS2Convert()
    {
        var basePath = Application.dataPath + "/gameres/";

        string[] ignores = {
            ".meta",
            ".tga",
            ".description", // text
            ".ds2aim", // binary without header
            ".phs", // text
            ".rds", // text
            ".ds2sn", // text
            ".msh", // model without header / maybe mdl patch
        };

        var extensions = new List<string>();
        var files = Directory.GetFiles(basePath, "*", SearchOption.AllDirectories);
        foreach (string path in files)
        {
            var ext = Path.GetExtension(path).ToLower();
            if (!ArrayUtility.Contains(ignores, ext))
                extensions.Add(path);
        }

        extensions.Sort();

		Dictionary<string, Dictionary<string, List<string>>> formats = new Dictionary<string, Dictionary<string, List<string>>>();

        foreach (string path in extensions)
        {
			//var file = new DS2File(path);
			//file.Close();

			//Dictionary<string, List<string>> formatDict;
			//if (formats.TryGetValue(file.format, out formatDict))
			//{
			//	List<string> filesList;
			//	if (formatDict.TryGetValue(file.version, out filesList))
			//	{
			//		filesList.Add(path);
			//	}
			//	else
			//	{
			//		filesList = new List<string>();
			//		filesList.Add(path);
			//		formatDict.Add(file.version, filesList);
			//	}
			//}
			//else
			//{
			//	formatDict = new Dictionary<string, List<string>>();
			//	var filesList = new List<string>();
			//	filesList.Add(path);
			//	formatDict.Add(file.version, filesList);
			//	formats.Add(file.format, formatDict);
			//}
        }

		foreach (KeyValuePair<string, Dictionary<string, List<string>>> kvp in formats)
		{
			Debug.Log(kvp.Key);

			foreach (KeyValuePair<string, List<string>> kvp2 in kvp.Value)
			{
				Debug.Log("\t" + kvp2.Key + ": " + kvp2.Value.Count);
				if (kvp2.Value.Count < 100)
				{
					foreach (string path in kvp2.Value)
						Debug.Log("\t\t" + path);
				}
			}
		}

		Debug.Log("complete");
    }
}